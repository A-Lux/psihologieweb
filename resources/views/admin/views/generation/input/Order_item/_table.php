<?php

$exegesis = [
    "status" => [
        "new" => "<b>Новый</b>",
        "done" => "Доставлен",
        "accepted" => "Заказ обработан",
        "return" => "Возврат",
        "cancelled" => "Отменить",
    ]
];

?>
