@php

    $product_id  = isset($_REQUEST["construction_id"])? $_REQUEST["construction_id"]:'';


    $product_id  = is_null($model)? $product_id: $model->construction_id;


    $typelist=[
        ['Адрес','point'],
        ['Телефон','phone'],
        ['Email','email'],
        ['Заголовок','title'],
        ['Инстаграм','inst'],
];
@endphp

<div class="row-fluid" style="margin-bottom: 1.5rem;">
    <select class="selectpicker" name="type_save" data-show-subtext="true" data-live-search="true">
        <option value="">Выберите категорию</option>
        @foreach($typelist as $catalog)
            <option value="{{$catalog[1]}}" {{ !is_null($model)?($model->type==$catalog[1]?'selected':'' )
                        : ''}} >{{$catalog[0]}}
            </option>
        @endforeach
    </select>
</div>


<input type="hidden" name="construction_id_save" value="{{$product_id}}">
<input type="hidden" name="path" value="{{url_custom("/admin/model/Construction/".$product_id)}}">
