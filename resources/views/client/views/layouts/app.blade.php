<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Laravel') }} </title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="user-token" content="">

    <link rel="stylesheet" href="/public/media/client/css/style.css?v=0.1">
    <meta name="description" content="">
    <meta name="Keywords" content="">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <style>
        .menu {
            position: fixed;
            height: 100%;
            width: 100%;
            z-index: 4;
            background-color: #fff;
            visibility: hidden;
            transition: 0s;
        }
    </style>
</head>

<body>


<div class="menu">
    <div class="menu_block">

        <header class="header" style="margin-bottom: 0">
            <div class="header_main container">
                <div class="header_main_desk">

                    <div class="menu_click">
                        <a href="javascript:void(0)" class="menu_icon menu_close " style=" transform: rotate(90deg); ">
                            <div></div>
                            <div></div>
                            <div></div>
                        </a>
                    </div>

                    <a href="/" class="logo logo-head">
                        <img src="/public/media/client/images/logo.png" alt=""> <span
                            class="text text-s18">B&C Burger</span>
                    </a>

                    <div style="margin-left: auto"></div>
                </div>
            </div>
        </header>

        <nav class="nav-catalog ">
            <?php
            $nav = \App\Catalog::get();
            ?>
            @foreach($nav as $na)
                <a href="{{url_custom('/'.$na->path)}}" class="nav-item">
                    <span class="text text-s20">{{$na->title}}</span>
                </a>
            @endforeach

        </nav>


    </div>
</div>

<main>
{{--    <header class="header">--}}
{{--        <div class="header_main container">--}}
{{--            <div class="header_main_desk">--}}


{{--                @if(isset($title))--}}
{{--                    <a href="/" class="btn btn-back">--}}
{{--                        <img src="/public/media/client/images/arrow_back.png" alt="">--}}
{{--                    </a>--}}

{{--                    <p class="title-head text text-s20">--}}
{{--                        {{$title}}--}}
{{--                    </p>--}}
{{--                @elseif(isset($basket))--}}
{{--                    <a href="/" class="btn btn-back">--}}
{{--                        <img src="/public/media/client/images/arrow_back.png" alt="">--}}
{{--                    </a>--}}

{{--                    <p class="title-head text text-s20">--}}
{{--                        {{$basket}}--}}
{{--                    </p>--}}
{{--                @else--}}
{{--                    <div class="menu_click">--}}
{{--                        <a href="javascript:void(0)" class="menu_icon menu_open">--}}
{{--                            <div></div>--}}
{{--                            <div></div>--}}
{{--                            <div></div>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                    <a href="/" class="logo logo-head">--}}
{{--                        <img src="/public/media/client/images/logo.png" alt=""> <span--}}
{{--                            class="text text-s18">B&C Burger</span>--}}
{{--                    </a>--}}
{{--                @endif--}}

{{--                @if(isset($basket))--}}
{{--                    <a href="javascript:void(0)" style=" margin-left: auto;color:#fff; "><span class="text text-s16">Очистить</span></a>--}}
{{--                @else--}}
{{--                    <nav class="nav">--}}
{{--                        <a href="{{url_custom('/basket')}}" class="btn btn-basket"> <img--}}
{{--                                src="/public/media/client/images/basket.png"--}}
{{--                                alt=""></a>--}}
{{--                        <a href="/" class="btn btn-zoom "> <img src="/public/media/client/images/zoom.png" alt=""></a>--}}
{{--                    </nav>--}}
{{--                @endif--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </header>--}}


    @yield('content')


</main>

<link href='https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.16.0/codemirror.css' rel='stylesheet'>
<!-- The link above loaded the core css -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.16.0/codemirror.js'></script>
<!-- The script above loaded the core editor -->

<script src='https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.16.0/mode/javascript/javascript.js'></script>

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script src="/public/media/client/js/app.js?v=0.1"></script>
</body>
</html>
