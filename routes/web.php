<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$locale = Request::segment(1);
$lang = true;





Route::group(['prefix' => url_routes() . '/admin'], function () {

    Auth::routes();
    Route::get('/', 'admin\MainController@index');
    Route::get('/dev/{dev_status}', 'admin\MainController@developer');
    Route::get('/table', 'admin\TablesController@index');
    Route::post('/table_meta_update', 'admin\TablesController@table_meta_update');
    Route::get('/table/{model_name}', 'admin\TablesController@edit');
    Route::get('/column_name', 'admin\TablesController@column');
    Route::get('/column_name/edit/{id}', 'admin\TablesController@column_edit');
    Route::post('/update', 'admin\UpdateController@update_model');

    Route::post('/orders/edit/set_status', 'admin\MainController@status_set');


    Route::get('model/{model_name}', 'admin\GenerationController@model_catalog');
    Route::post('model/{model_name}', 'admin\GenerationController@model_position');
    Route::get('model/{model_name}/{id}', 'admin\GenerationController@model_save');
    Route::get('model/{model_name}/{id}/remove', 'admin\GenerationController@model_remove');
    Route::get('model/{model_name}/{id}/hidden/{visable}', 'admin\GenerationController@model_hidden');

    Route::get('StaticText', 'admin\MainController@s_text');
    Route::get('export', 'admin\MainController@export_ex');
    Route::get('export/{id}', 'admin\MainController@export_one_ex');
    Route::post('update_text', 'admin\MainController@update_text');

});


//Route::middleware('auth:api')->group( function () {
//    Route::resource('products', 'API\ProductController');
//});
Route::group(['prefix' => url_routes() . '/'], function () {

    Auth::routes();
    Route::get('', 'client\MainController@index');
    Route::get('{path}/p-{product}', 'client\MainController@product')->where('path', '[ а-яА-ЯЁёa-zA-Z0-9/_-]+');
    Route::get('{path}', 'client\MainController@catalog')->where('path', '[а-яА-ЯЁёa-zA-Z0-9\-/_\[\].┼♪♫; ]+');

});


