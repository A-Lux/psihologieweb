<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function items()
    {
        return $this->hasMany('App\Order_item', 'orders_id', "id");
    }

    public function getStatusAttribute($value)
    {
        $ststus = [
            "new" => "Новый",
            "done" => "Доставлен",
            "accepted" => "Заказ обработан",
            "return" => "Возврат",
            "cancelled" => "Отменить",
        ];
        return $ststus[$value];
    }
}
