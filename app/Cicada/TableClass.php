<?php


namespace App\Bulp;

use Exception;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Storage;

class TableClass
{
    protected $table;
    protected $columns;

    function __construct($table, $columns = [])
    {
        $this->table = $table;
        $this->columns = [];
        foreach ($columns as $colum) {
            if (isset($colum["name"])) {
                if (!isset($colum["type"])) {
                    $colum["type"] = "string";
                }
                if (!isset($colum["nullable"])) {
                    $colum["nullable"] = true;
                }
                array_push($this->columns,
                    $colum
                );
            }
        }
    }
    function save()
    {
        $this->finish();
        try {
            if (Schema::hasTable($this->table)) {
                $this->edit();
            } else {
                $this->create();
            }
        } catch (Exception $e) {
            echo("error");
        }
    }
    function edit()
    {
        Schema::table($this->table, function ($table) {
            foreach ($this->columns as $column) {
                $null = "";
                if ($column["type"] == true) {
                    $null = "->nullable()";
                }
                eval('$table->' . $column["type"] . "('" . $column["name"] . "')" . $null . ";");
            }
        });
        $this->finish();
    }
    function create()
    {

        Schema::create($this->table, function (Blueprint $table) {
            foreach ($this->columns as $column) {
                $null = "";
                if ($column["type"] == true) {
                    $null = "->nullable()";
                }
                eval('$table->' . $column["type"] . "('" . $column["name"] . "')" . $null . ";");
            }
            $table->timestamps();
        });

        $this->finish();

    }
    function finish()
    {
        $path = storage_path() . "/TableClass/" . $this->table . ".json";
        $tableJson = [];
        if (file_exists($path)) {
            $tableJson = json_decode(file_get_contents($path), true);
            if (!is_array($tableJson)) {
                $tableJson = [];
            }
           unlink($path);
        }


        foreach ($this->columns as $column) {
            foreach ($tableJson as $keyx => $columnAlt) {

                if (isset($columnAlt["name"]) && isset($column["name"])) {
                    if ($columnAlt["name"] == $column["name"]) {
                        unset($tableJson[$keyx]);
                    }
                }
            }
            array_push($tableJson, $column);
            sort($tableJson);

        }

        $handle = fopen($path, 'w');
        fwrite($handle, json_encode($tableJson));
        fclose($handle);
    }
}

