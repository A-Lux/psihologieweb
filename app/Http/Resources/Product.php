<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $data=[
            'id' => $this->id,
            'title' => $this->title,
            'images' => $this->images,
            'ed_massa' => $this->ed_massa,
            'price' => $this->price,
            'catalog_id' => $this->catalog_id,
            'content' => $this->content,
            'created_at' => $this->created_at->format('d/m/Y'),
            'updated_at' => $this->updated_at->format('d/m/Y'),
        ];
        if (isset($this->fullscreen)) {
            $data["fullscreen"] = $this->fullscreen;
        }
        if (isset($this->amount)) {
            $data["amount"] = $this->amount;
        }
        if (isset($this->basket_id)) {
            $data["basket_id"] = $this->basket_id;
        }

        return $data;
    }
}
