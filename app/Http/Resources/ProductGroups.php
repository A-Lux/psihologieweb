<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ProductGroupsItem as ProductGroupsItemResource;
class ProductGroups extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        \App\ProductGroupsItem::where("product_groups_id",$this->id);
        return [
            'id' => $this->id,
            'name' => $this->name,
            'created_at' => $this->created_at->format('d/m/Y'),
            'products'=>ProductGroupsItemResource::collection($this->items)
        ];
    }
}
