<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Model_list extends Model
{
    protected $fillable = [
        'name'
    ];

    public function meta($model_type)
    {
        return $this->hasMany('App\Model_meta','attachment',"name_key")->where("type",$model_type);
    }

    public function availability()
    {
        return $this->hasMany('App\Model_meta','attachment',"name_key")->where("type","table_catalog_availability");
    }

}
