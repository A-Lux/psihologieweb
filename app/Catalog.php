<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    public function products()
    {
        return $this->hasMany('\App\Product')->where("visable", "1")->orderby("sort");
    }

    public function children()
    {
        return $this->hasMany('\App\Catalog','parent_id',"id");
    }
}
