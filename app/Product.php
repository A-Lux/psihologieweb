<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function getImagesAttribute($value)
    {
        return 'https://' . $_SERVER['SERVER_NAME'] . $value;
    }

    public function getContentAttribute($value)
    {
        return strip_tags(str_replace("&nbsp;", ' ', $value));
    }

    public function getPriceAttribute($value)
    {
        return str_replace(["-", "+", " ","тг"], "", filter_var($value, FILTER_SANITIZE_NUMBER_INT));
    }
}
