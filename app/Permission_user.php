<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
class Permission_user extends Model
{
    //

    public static function check(){

        $userDate=self::where("user_id",Auth::user()->id)->first();

        if(isset($userDate)){
            return $userDate->grup_id;
        }

        return -1;
    }

    public static function checkId($id){
        $userDate=self::where("user_id",$id)->first();

        if(isset($userDate)){
            return $userDate->grup_id;
        }

        return -1;
    }

}
