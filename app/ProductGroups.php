<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductGroups extends Model
{
    public function items()
    {
        return $this->hasMany('\App\ProductGroupsItem');
    }
}
